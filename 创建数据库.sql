create database if not exists member default charset utf8;

use member;

create table if not exists t_member
(
    id int primary key auto_increment,
    name varchar(20) not null comment "会员名",
    phone varchar(15) UNIQUE comment "会员电话",
    point int default 0 comment "会员积分",
    consume double default 0 comment "消费金额",
    change_point timestamp default current_timestamp on update current_timestamp comment "积分修改时间",
    create_time timestamp default current_timestamp comment "用户创建时间"
)engine InnoDB default charset utf8;


