﻿#ifndef WIDGET_H
#define WIDGET_H
#pragma once
#include "msg.h"
#include <QWidget>
#include <QMessageBox>
#include <QDebug>
#include <QSqlDatabase>//========
#include <QSqlQuery>
#include <QSqlQueryModel>
#include <QItemSelectionModel>
#include <QString>
#include <QSqlError>
#include <QSqlRecord>//database
#include <QTimer>
#include <QJsonArray> //========
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonParseError>
#include <QList>
#include <QByteArray> //json
#include <QUrl>
#include <QNetworkAccessManager>//network
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QSslConfiguration>
#include <QRegExp>
#include <QTextCodec>
#include <QList>
#define DATABASE_NAME "member"
#define TABLE_NAME "t_member"
namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private slots:

    void on_pushButton_dbClose_clicked();  //关闭数据库连接

    void on_pushButton_dbCon_clicked();  //连接数据库

    void on_pushButton_register_clicked();  //注册用户

    void on_pushButton_readModel_clicked();//读取短信模块
    void on_timeOut();
    void on_pushButton_checkPoint_clicked();//查询用户积分

    void on_pushButton_expend_clicked();
    void tochange(QModelIndex modIndex);//跳页修改
    void findTel(QString str);//模糊查找
    void clearTel();//换页清空(总)
    void lookMemClear();//查询清空(分)
    void expendClear();//消费记录清空(分)
    void pointChangeClear();//积分清空(分)
    void addTel();//双击跳转增加
    void on_pushButton_pointWrite_clicked();//修改记录
    void on_pushButton_delMember_clicked();//删除用户

    void on_pushButton_send_clicked();//选择tel
    void on_readNum();
    void on_finish();
    void on_readModel();

    void on_comboBox_id_currentIndexChanged(const QString &arg1);

    void on_pushButton_last_clicked();

    void on_pushButton_sendFromTel_clicked();
    void on_readSendStatus();
private:
    Ui::Widget *ui;
private:
    QSqlDatabase db;
    QSqlQueryModel *pQueryModel;
    QItemSelectionModel *pSelection;
    bool flag;
    QTimer timer;
    QNetworkAccessManager manager;
    //QNetworkAccessManager manager_model;
    //QNetworkReply *pReply;
    //QNetworkReply *pReply_readModel;
    QNetworkRequest request;
    //QNetworkRequest request_readModel;
    QSslConfiguration ssl; //用于https网络请求
    QString str_baseAdd;
    QString str_type;
    QString str_uid;
    QString str_pwd;
    QString str_ext;//额外的属性
    QList<Msg> msgList;
    QList<QNetworkReply*> replies;
private:
    void openTable();
    bool ishave(QString str);
    bool isNumber(QString str);
};

#endif // WIDGET_H
