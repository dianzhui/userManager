﻿#include "widget.h"
#include "ui_widget.h"

#if _MSC_VER >=1600
#pragma execution_character_set("utf-8")
#endif

Widget::Widget(QWidget *parent) :
    QWidget(parent),ui(new Ui::Widget),str_baseAdd("https://api.yunduanxin.com/sms/?ac="),
    str_uid("&uid="),flag(false),str_pwd("&pwd="),str_type(""),str_ext("&content=")
{
    ui->setupUi(this);
    ui->tabWidget->setCurrentIndex(0);
    ui->pushButton_dbClose->setEnabled(false);
    ui->pushButton_dbClose->setStyleSheet("QPushButton{color:rgb(192,192,192)}");
    this->setWindowTitle("武汉加油会员管理系统");
    ui->lineEdit_dbAddress->setText("127.0.0.1"); //本地测试使用的mysql数据库地址，线上请使用远程数据库。
    ui->lineEdit_dbUser->setText("root"); //仅测试使用，线上请添加数据库用户，并设置相关权限。
    ui->lineEdit_dbPasswd->setText("123456"); //仅测试使用
    ui->lineEdit_dbPort->setText("3306"); //mysql数据库一般都是3306端口，也可以在数据库设置里更改端口号，一般不建议修改。
    ui->lineEdit_dbName->setText(DATABASE_NAME); //这里设置数据库名
    //db.addDatabase("QMYSQL");//这样设置会出错
    // 加载mysql驱动
    db=QSqlDatabase::addDatabase("QMYSQL");
    //https请求，需要进行的相关的设置
    ssl.setPeerVerifyMode(QSslSocket::VerifyNone);
    ssl.setProtocol(QSsl::TlsV1SslV3);
    request.setSslConfiguration(ssl);
    //这里是设置http请求头来模拟浏览器请求，防止对方有反爬机制(虽然知道对方没有...)。
    //没错，我之前就用这个做爬虫，爬过一些小电影。。。，当然了，还是用python更方便和快捷一点。
    request.setRawHeader("User-Agent","Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36");

    msgList.clear();

    /*******************信号集**************************/


    //查询积分的信号(分)
    connect(ui->lineEdit_lookMemTel,SIGNAL(textChanged(QString)),this,SLOT(lookMemClear()));
    //消费清空（分）
    connect(ui->lineEdit_expendTel,SIGNAL(textChanged(QString)),this,SLOT(expendClear()));
   //积分清空(分)
    connect(ui->lineEdit_pointChangeTel,SIGNAL(textChanged(QString)),this,SLOT(pointChangeClear()));
    //跳页修改
    connect(ui->tableView,SIGNAL(doubleClicked(QModelIndex)),this,SLOT(tochange(QModelIndex)));
    //模糊查找
    connect(ui->lineEdit_findTel,SIGNAL(textChanged(QString)),this,SLOT(findTel(QString)));
    //模糊双击跳转
    connect(ui->lineEdit_findTel,SIGNAL(selectionChanged()),this,SLOT(addTel()));
    //换页清空(总)
    connect(ui->tabWidget,SIGNAL(currentChanged(int)),this,SLOT(clearTel()));
}

Widget::~Widget()
{
    delete ui;
}


void Widget::openTable()//打开表
{
    /***********根据编辑框的填空来设置数据库相关的属性**************/
    db.setHostName(ui->lineEdit_dbAddress->text());
    db.setUserName(ui->lineEdit_dbUser->text());
    db.setPassword(ui->lineEdit_dbPasswd->text());
    db.setPort(ui->lineEdit_dbPort->text().toInt());
    db.setDatabaseName(ui->lineEdit_dbName->text());
    if(!db.open()) //打开数据库失败
    {
        QMessageBox::warning(this,"错误","数据库连接失败,请检查配置信息!\nerror:"+db.lastError().text());
        return;
    }
    else //打开数据库成功
    {
        flag=true;
        ui->label_dbStatu->setText("数据库连接成功!");
        ui->label_dbStatu->setStyleSheet("QLabel{color:rgb(0,132,33)}");
    }
    ui->pushButton_dbCon->setEnabled(false);//使能
    ui->pushButton_dbCon->setStyleSheet("QPushButton{color:rgb(192,192,192)}");
    ui->pushButton_dbClose->setEnabled(true);
    ui->pushButton_dbClose->setStyleSheet("QPushButton{color:rgb(175,80,63)}");
    //MVC模式中的modul
    pQueryModel=new QSqlQueryModel(this);
    pQueryModel->setQuery("select * from "+QString::fromLocal8Bit(TABLE_NAME));
    //设置表头
    pQueryModel->setHeaderData(0,Qt::Horizontal,"编号");
    pQueryModel->setHeaderData(1,Qt::Horizontal,"会员名");
    pQueryModel->setHeaderData(2,Qt::Horizontal,"会员电话");
    pQueryModel->setHeaderData(3,Qt::Horizontal,"会员积分");
    pQueryModel->setHeaderData(4,Qt::Horizontal,"消费金额");
    pQueryModel->setHeaderData(5,Qt::Horizontal,"积分修改时间");
    pQueryModel->setHeaderData(6,Qt::Horizontal,"用户创建时间");
    //MVC中的control
    pSelection=new QItemSelectionModel(pQueryModel);
    //MVC中的view，再次感叹下，这设计模式真方便。
    //设置下表格的宽度
    ui->tableView->setModel(pQueryModel);
    ui->tableView->setSelectionModel(pSelection);
    ui->tableView->setColumnWidth(0,50);
    ui->tableView->setColumnWidth(1,60);
    ui->tableView->setColumnWidth(2,90);
    ui->tableView->setColumnWidth(3,80);
    ui->tableView->setColumnWidth(4,80);
    ui->tableView->setColumnWidth(5,110);
    ui->tableView->setColumnWidth(6,110);

}



void Widget::on_pushButton_dbClose_clicked()//关闭数据库
{
    db.close();
    flag=false;
    ui->pushButton_dbCon->setEnabled(true);
    ui->pushButton_dbCon->setStyleSheet("QPushButton{color:rgb(79,192,46)}");
    ui->label_dbStatu->setText("数据库未连接");
    ui->label_dbStatu->setStyleSheet("QLabel{color:rgb(255,0,0)}");
    ui->pushButton_dbClose->setEnabled(false);
    ui->pushButton_dbClose->setStyleSheet("QPushButton{color:rgb(192,192,192)}");
    pQueryModel->clear();
}

void Widget::on_pushButton_dbCon_clicked()//连接数据库
{
    if(ui->lineEdit_dbUser->text()=="" || ui->lineEdit_dbPasswd->text()==""
            ||ui->lineEdit_dbPort->text()==""|| ui->lineEdit_dbName->text()=="")
    {
        QMessageBox::warning(this,"数据库连接错误!","数据库参数填写不完整，请检查.");
        return;
    }
    openTable();
    ui->tableView->setFocus();
}


void Widget::on_pushButton_readModel_clicked()//读取短信模块内容
{
    if(ui->lineEdit_uid->text()=="" || ui->lineEdit_pwd->text()=="")
    {
        QMessageBox::warning(this,"错误","用户名和密码请填写完整!");
        return;
    }
    QString uid= str_uid+ui->lineEdit_uid->text();
    QString pwd=str_pwd+ui->lineEdit_pwd->text();
    /************开始请求了，分三步走**************/
    //1.构造http请求的url
    QUrl url_model=QUrl::fromUserInput(str_baseAdd+"templatequery"+uid+pwd+"&page=1");
    //2.将url设置进请求对象request中
    request.setUrl(url_model);
    //3.利用网络访问管理器发出get请求，当然了还有post请求，put请求之类的。
    QNetworkReply* preply=manager.get(request);
    replies.append(preply);
    connect(preply,SIGNAL(readyRead()),this,SLOT(on_readModel()));
    connect(preply,SIGNAL(finished()),this,SLOT(on_finish()));
}

void Widget::on_timeOut()//超时处理
{

    return;
}



void Widget::on_pushButton_register_clicked()//注册会员
{
    if(""==ui->lineEdit_memberName->text() || ""==ui->lineEdit_memberTel->text())
    {
        QMessageBox::warning(this,"警告","注册信息请填写完整!");
        return;
    }
    if(!flag || !db.isOpen())//检查数据库状态
    {
        QMessageBox::warning(this,"警告","请先连接数据库!!!");
        return;
    }
    if(isNumber(ui->lineEdit_memberTel->text()))
    {
        if(!ishave(ui->lineEdit_memberTel->text()))//是否存在
        {
            QSqlQuery squ;
            squ.prepare("insert into "+QString::fromLocal8Bit(TABLE_NAME)+"(phone,name) values(:tel,:name)");
            squ.bindValue(":tel",ui->lineEdit_memberTel->text());
            squ.bindValue(":name",ui->lineEdit_memberName->text());
            if(!squ.exec())
            {
                QMessageBox::warning(this,"警告","加入用户失败,error:\n"+squ.lastError().text());
                return;
            }
            else
            {
                QMessageBox::information(this,"注册成功-_-","恭喜,会员注册成功!  -_-");
                ui->lineEdit_memberTel->clear();
                ui->lineEdit_memberName->clear();
            }
            pQueryModel->setQuery("select * from "+QString::fromLocal8Bit(TABLE_NAME));//刷新表格
        }
        else
        {
            QMessageBox::warning(this,"警告！","该用户已存在!!!");
            return;
        }
    }
    else
    {
        QMessageBox::warning(this,"警告！","号码格式不正确!");
        return;
    }
}

void Widget::on_pushButton_checkPoint_clicked()//查询积分
{

    if(!flag)//检查数据库状态
    {
        QMessageBox::warning(this,"警告","请先打开数据库!!!");
        return;
    }
    if(""==ui->lineEdit_lookMemTel->text())
    {
        QMessageBox::warning(this,"错误","请输入手机号码!");
        return;
    }
    QSqlQuery query;
    query.prepare("select * from "+QString::fromLocal8Bit(TABLE_NAME)+" where phone = :tel");
    query.bindValue(":tel",ui->lineEdit_lookMemTel->text());
    if(query.exec())//执行成功
    {
        query.first(); //将游标移到第一项
        if(query.value("point").isValid() && query.value("consume").isValid())
        {
            ui->lineEdit_lookMemPoint->setText(query.value("point").toString());
            ui->lineEdit_lookMemberExpend->setText(query.value("consume").toString());
        }
        else
        {
            QMessageBox::warning(this,"错误","查无此号，请核实!");
            return;
        }

    }
    else
    {
        QMessageBox::warning(this,"错误","error:"+query.lastError().text());
        return;
    }
}

void Widget::on_pushButton_expend_clicked()//消费记录
{
    if(!flag || !db.isOpen())//检查数据库状态
    {
        QMessageBox::warning(this,"警告","请先打开数据库!!!");
        return;
    }
    if(""==ui->lineEdit_expendTel->text() || ""==ui->lineEdit_expend->text() ||
            ""==ui->lineEdit_ponitWrite->text())
    {
        QMessageBox::warning(this,"错误","请输入手机号码,消费金额和积分!");
        return;
    }

    if(!ishave(ui->lineEdit_expendTel->text()))
    {
        QMessageBox::warning(this,"错误","无此会员，请核实！！");
        return;
    }
    else
    {

            if(ui->lineEdit_ponitWrite->text().toInt()>=0 && ui->lineEdit_expend->text().toDouble()>=0)
            {
                QSqlQuery query;
                query.prepare("select * from "+QString::fromLocal8Bit(TABLE_NAME)+" where phone=:tel");
                query.bindValue(":tel",ui->lineEdit_expendTel->text());
                query.exec();
                query.first(); //好坑
                int n_point = ui->lineEdit_ponitWrite->text().toInt();
                double num = ui->lineEdit_expend->text().toDouble();
                n_point += query.value("point").toInt();
                num += query.value("consume").toDouble();
                query.clear();//清空，准备更新消费金额
                query.prepare("update "+QString::fromLocal8Bit(TABLE_NAME)+" set consume=:num,point=:point where phone=:tel");
                query.bindValue(":num",num);
                query.bindValue(":point",n_point);
                query.bindValue(":tel",ui->lineEdit_expendTel->text());

                if(query.exec())
                {

                    ui->lineEdit_expendTel->clear();
                    ui->lineEdit_expend->clear();
                    ui->lineEdit_ponitWrite->clear();
                    QMessageBox::information(this,"录入成功","消费金额和会员积分已成功录入! -_-");
                    pQueryModel->setQuery("select * from "+QString::fromLocal8Bit(TABLE_NAME));//刷新表格
                }
                else
                {
                    QMessageBox::warning(this,"错误","消费金额和积分加入失败!\nerror:"+query.lastError().text());
                    return;
                }
            }
            else
            {
                QMessageBox::warning(this,"错误","数据不能为负数!");
                return;
            }


    }
}

void Widget::on_pushButton_pointWrite_clicked() //修改记录
{
    if(!flag || !db.isOpen())//检查数据库状态
    {
        QMessageBox::warning(this,"警告","请先打开数据库!!!");
        return;
    }
    if(""==ui->lineEdit_pointChangeTel->text() || ""==ui->lineEdit_expendChange->text() ||
            ""==ui->lineEdit_ponitChange->text())
    {
        QMessageBox::warning(this,"错误","请输入手机号码,需要修改的金额和积分!");
        return;
    }
    if(ishave(ui->lineEdit_pointChangeTel->text()))
    {
        QSqlQuery query;
        double num=ui->lineEdit_expendChange->text().toDouble();
        int n_point=ui->lineEdit_ponitChange->text().toInt();
        if(num>=0 && n_point>=0)
        {
            query.prepare("update "+QString::fromLocal8Bit(TABLE_NAME)+" set consume=:num,point=:point  where phone=:tel");
            query.bindValue(":num",num);
            query.bindValue(":point",n_point);
            //query.bindValue(":datetime",QDateTime::currentDateTime().toString());
            query.bindValue(":tel",ui->lineEdit_pointChangeTel->text());
            if(query.exec())
            {
                ui->lineEdit_pointChangeTel->clear();
                ui->lineEdit_expendChange->clear();
                ui->lineEdit_ponitChange->clear();
                QMessageBox::information(this,"成功","修改数据成功！-_-");
                pQueryModel->setQuery("select * from "+QString::fromLocal8Bit(TABLE_NAME));//刷新表格
            }
            else
            {
                QMessageBox::warning(this,"错误","修改数据失败!\nerror:"+
                                 query.lastError().text());
                return;
            }
        }

        else
        {
            QMessageBox::warning(this,"错误","消费金额和积分不能小于0!");
            return;
        }
    }
    else
    {
        QMessageBox::warning(this,"错误","无此会员，请核实！！");
        return;
    }

}

void Widget::on_pushButton_delMember_clicked()//删除用户
{
    if(!flag)//检查数据库状态
    {
        QMessageBox::warning(this,"警告","请先打开数据库!!!");
        return;
    }
    if(""==ui->lineEdit_delMember->text())
    {
        QMessageBox::warning(this,"警告","请先填写用户的电话号码!");
        return;
    }
    if(ishave(ui->lineEdit_delMember->text()))
    {
        if(QMessageBox::Ok==QMessageBox::warning(this,"警告!","你确定要删除此会员吗？",QMessageBox::Ok|QMessageBox::Cancel))
        {
            QSqlQuery query;
            query.prepare("delete from "+QString::fromLocal8Bit(TABLE_NAME)+" where phone = :tel");
            query.bindValue(":tel",ui->lineEdit_delMember->text());
            if(query.exec())
            {
                ui->lineEdit_delMember->clear();
                //QMessageBox::information(this,"成功","成功删除该会员!");
                pQueryModel->setQuery("select * from "+QString::fromLocal8Bit(TABLE_NAME));//刷新表格
            }
            else
            {
                QMessageBox::warning(this,"错误","删除会员失败!\nerror:"+
                                 query.lastError().text());
                return;
            }
        }
    }
    else
    {
        QMessageBox::warning(this,"错误","无此号码用户，请核实!");
        return;
    }
}



bool Widget::ishave(QString str)//记录是否存在
{
    if(!flag || !db.isOpen())//检查数据库状态
    {
        QMessageBox::warning(this,"警告","请先打开数据库!!!");
        return false;
    }
    QSqlQuery query; //查询语句
    query.prepare("select * from "+QString::fromLocal8Bit(TABLE_NAME)+" where phone = :tel");
    query.bindValue(":tel",str);
    if(query.exec())
    {
        query.first();
        if(query.isValid())
            return true;
        else
            return false;
    }
    else
    {
        QMessageBox::warning(this,"错误","执行失败!\nerror:"+query.lastError().text());
        return false;
    }

}

bool Widget::isNumber(QString str) //检查用户输入的手机号码是否满足正则匹配，并且是否为11位。
{
    QRegExp reg("^[0-9]{11}$");
    if(reg.exactMatch(str) && reg.matchedLength()==11)
        return true;
    else
        return false;
}

void Widget::on_pushButton_send_clicked()//选择tel
{
    if(flag)
    {
        if(!pSelection->selectedIndexes().isEmpty())
        {

            for(int i=0;i<pSelection->selectedRows().size();++i)
            {

                int index = pSelection->selectedIndexes()[i*6].row();//下标
                ui->listWidget_tel->addItem(pQueryModel->record(index).value("phone").toString());
            }
            ui->tabWidget->setCurrentIndex(2);
        }
        else
        {
            QMessageBox::warning(this,"error","请先在表格中选择一个或多个用户");
        }
    }
}

void Widget::on_pushButton_last_clicked()//短信条数
{
    if(ui->lineEdit_uid->text()=="" || ui->lineEdit_pwd->text()=="")
    {
        QMessageBox::warning(this,"错误","用户名和密码请填写完整!");
        return;
    }
    ui->lineEdit_last->clear();
    QString uid= str_uid+ui->lineEdit_uid->text();
    QString pwd=str_pwd+ui->lineEdit_pwd->text();
    QUrl url_query=QUrl::fromUserInput(str_baseAdd+"number"+uid+pwd);
    request.setUrl(url_query);
    QNetworkReply* pReply=manager.get(request);
    //加入列表中
    replies.append(pReply);
    connect(pReply,SIGNAL(readyRead()),this,SLOT(on_readNum()));
    connect(pReply,SIGNAL(finished()),this,SLOT(on_finish()));
}


void Widget::on_pushButton_sendFromTel_clicked()//确认发送
{
    if(ui->lineEdit_uid->text()!="" && ui->lineEdit_pwd->text()!="" && ui->listWidget_tel->count()>0)
    {
        QString tmp,str_extend="&content=";
        QTextCodec *gbk=QTextCodec::codecForName("gbk");
        QString uid= str_uid+ui->lineEdit_uid->text();
        QString pwd=str_pwd+ui->lineEdit_pwd->text();
        QString str_url=str_baseAdd+"send"+uid+pwd+"&mobile=";

        for(int i=0;i<ui->listWidget_tel->count();++i)
        {
            if(i==0)
                tmp=ui->listWidget_tel->item(i)->text();
            else
                tmp=","+ui->listWidget_tel->item(i)->text();
            str_url+=tmp;
        }
        str_extend+=QString(gbk->fromUnicode(ui->textEdit_testEdit->toPlainText()).toPercentEncoding().replace("%20","+"));
        QUrl url_query=QUrl::fromUserInput(str_url+str_extend);
        request.setUrl(url_query);
        QNetworkReply* preply=manager.get(request);
        replies.append(preply);
        connect(preply,SIGNAL(readyRead()),this,SLOT(on_readSendStatus()));
        connect(preply,SIGNAL(finished()),this,SLOT(on_finish()));
    }
    else
    {
        QMessageBox::warning(this,"错误","用户名，密码和发送号码请检查完整性!");
        return;
    }
}


/************************槽函数*******************************/
void Widget::tochange(QModelIndex modIndex)//表格里双击
{
    QString tel=pQueryModel->record(modIndex.row()).value("phone").toString();
    ui->tabWidget->setCurrentIndex(1);
    ui->lineEdit_expendTel->clear();//消费电话
    ui->lineEdit_pointChangeTel->clear();//积分修改电话
    ui->lineEdit_lookMemTel->clear();//查找
    ui->lineEdit_delMember->clear();//删除
    ui->lineEdit_expendTel->setText(tel);
    ui->lineEdit_pointChangeTel->setText(tel);
    ui->lineEdit_lookMemTel->setText(tel);
    ui->lineEdit_delMember->setText(tel);
}

void Widget::findTel(QString str)//模糊查找
{
    if(flag)
    {
        QString str_find="select * from "+QString::fromLocal8Bit(TABLE_NAME)+
                " where phone like '"+str+"%'";
        pQueryModel->setQuery(str_find);
    }
}

void Widget::clearTel()//换页清空
{
    ui->lineEdit_expendTel->clear();
    ui->lineEdit_lookMemTel->clear();
    ui->lineEdit_delMember->clear();
    ui->lineEdit_pointChangeTel->clear();
}

void Widget::lookMemClear()//查询清空(分)
{
    ui->lineEdit_lookMemberExpend->clear();
    ui->lineEdit_lookMemPoint->clear();
}

void Widget::expendClear()//消费记录清空(分)
{
    ui->lineEdit_expend->clear();
    ui->lineEdit_ponitWrite->clear();
}

void Widget::pointChangeClear()//积分清空(分)
{
    ui->lineEdit_expendChange->clear();
    ui->lineEdit_ponitChange->clear();
}

void Widget::addTel()//双击跳转增加
{
    if(ui->lineEdit_findTel->text().length()==11) //防止误操作
    {
        ui->tabWidget->setCurrentIndex(1);
        ui->lineEdit_memberTel->setText(ui->lineEdit_findTel->text());
    }
}


void Widget::on_readNum()
{
    QObject* obj = sender();
    QNetworkReply* preply = Q_NULLPTR;
    foreach(auto rep,replies)
    {
        if(rep==obj)
        {
            preply = rep;
            obj = Q_NULLPTR;
            break;
        }
    }
    QByteArray byteArr=preply->readAll();
    QJsonParseError jsonErr;
    QJsonDocument doc=QJsonDocument::fromJson(byteArr,&jsonErr);
    if(jsonErr.error==QJsonParseError::NoError)
    {
        if(doc.isObject())
        {
            QJsonObject obj=doc.object();
            if(obj.contains("stat"))
            {
                if(obj.take("stat").toString()=="100")
                {
                    ui->lineEdit_last->setText(obj.take("number").toString());
                }
                else
                {

                    QMessageBox::warning(this,"","状态错误,err:\n"+obj.take("message").toString());
                    return;
                }
            }
            else
            {
                QMessageBox::warning(this,"","获取状态失败!");
                return;
            }
        }
    }
    else
    {
       QMessageBox::warning(this,"","json格式不对");
       return;
    }
    preply = Q_NULLPTR;  //不加也可，但这是好的习惯
}


void Widget::on_finish()
{
    QObject* obj = sender();
    foreach(auto rep,replies)
    {
        if(rep==obj)
        {
            rep->deleteLater();
            rep = Q_NULLPTR;
            replies.removeOne(rep);
            break;
        }
    }
    obj=Q_NULLPTR;
}

void Widget::on_readModel()
{
    QObject* obj = sender();
    QNetworkReply* preply = Q_NULLPTR;
    foreach(auto rep,replies)
    {
        if(rep==obj)
        {
            preply = rep;
            obj = Q_NULLPTR;
            break;
        }
    }
    QByteArray byteArr= preply->readAll();
    QJsonParseError jsonErr;
    QJsonDocument doc=QJsonDocument::fromJson(byteArr,&jsonErr);
    if(jsonErr.error==QJsonParseError::NoError)
    {
        if(doc.isObject())
        {
            QJsonObject obj=doc.object();
            Msg message;

            if(obj.contains("stat") && obj.contains("total"))
            {
                message.str_stat=obj.take("stat").toString();
                message.str_total=obj.take("total").toString();
            }
            if(obj.contains("values"))//是否有此字段
            {
                if(obj.value("values").isArray())//是否为数组(方括号),也就是列表
               {
                   QJsonArray jsArr=obj.value("values").toArray();
                   msgList.clear(); //清除模板列表，防止多次读取造成重复。
                   ui->comboBox_id->clear();
                   for(int i=0;i<jsArr.size();++i)
                   {
                       message.str_type=jsArr[i].toObject().take("type").toString();
                       message.str_dataformat=jsArr[i].toObject().take("dataformat").toString();
                       message.str_addtime=jsArr[i].toObject().take("addtime").toString();
                       message.str_id=jsArr[i].toObject().take("templateid").toString();
                       message.str_content=jsArr[i].toObject().take("content").toString();
                       message.str_question=jsArr[i].toObject().take("question").toString();
                       message.str_status=jsArr[i].toObject().take("status").toString();
                       ui->comboBox_id->addItem(message.str_id);
                       msgList.append(message);
                   }
                   ui->comboBox_id->setCurrentIndex(-1);
                }
             }
             else
             {
                 //qDebug()<<"无此字段";
             }

         }
         //else
             //qDebug()<<"不是对象";
     }
     else
     {
        QMessageBox::warning(this,"","json格式不对");
        return;
     }
    preply = Q_NULLPTR;
}




void Widget::on_comboBox_id_currentIndexChanged(const QString &arg1)
{
    if(ui->comboBox_id->currentText()!="")
    {
        for(int i=0;i<msgList.size();++i)
        {
            if(msgList[i].str_id==arg1)
            {
                if(msgList[i].str_dataformat=="1")//全文模板
                {
                    ui->textEdit_allEdit->setText(msgList[i].str_content);
                }
                else//json模板
                {
                    ui->textEdit_jsonEdit->setText(msgList[i].str_content);
                }
                ui->lineEdit_stat->setText(msgList[i].str_stat);
                ui->lineEdit_total->setText(msgList[i].str_total);
                ui->lineEdit_type->setText(msgList[i].str_type);
                ui->lineEdit_dataformat->setText(msgList[i].str_dataformat);
                ui->lineEdit_addtime->setText(msgList[i].str_addtime);
                ui->lineEdit_id->setText(msgList[i].str_id);
                ui->textEdit_question->setText(msgList[i].str_question);
                ui->lineEdit_status->setText(msgList[i].str_status);
            }
        }
    }
}

void Widget::on_readSendStatus()//短信发送状态
{
    QObject* obj = sender();
    QNetworkReply* preply = Q_NULLPTR;
    foreach(auto rep,replies)
    {
        if(rep==obj)
        {
            preply = rep;
            break;
        }
    }
    QByteArray byteArr = preply->readAll();
    QString str(byteArr);
    QMessageBox::information(this,"返回","返回状态:"+str);
}

