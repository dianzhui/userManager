﻿#ifndef MSG_H
#define MSG_H
#include <QString>
class Msg
{
  public:
    QString str_stat;//接口状态，100是成功
    QString str_total;//模板总数
    QString str_type; //类型 1.验证码 2.通知 3.其他
    QString str_dataformat;//格式，1.全文变量模板 2.json变量模板
    QString str_addtime;//添加时间
    QString str_id;//模板ID
    QString str_content;//模板内容
    QString str_question;//模板的问题
    QString str_status;//模板验证状态 0.审核成功 1.审核中 2.不通过(查看question)

};
#endif // MSG_H
