﻿#include "login.h"
#include "ui_login.h"

#if _MSC_VER >=1600
#pragma execution_character_set("utf-8")
#endif

Login::Login(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Login)
{
    ui->setupUi(this);
    this->setWindowTitle("XXX管理员登陆界面");
    connect(ui->pushButton_login,SIGNAL(clicked(bool)),this,SLOT(login()));
    connect(ui->pushButton_close,SIGNAL(clicked(bool)),this,SLOT(close()));
}

Login::~Login()
{
    delete ui;
}

void Login::login()
{
    //这里没有设置密码，直接回车即可。上线考虑使用mysql数据库验证密码。
    if(ui->lineEdit_userName->text()==""&&ui->lineEdit_passwd->text()=="")
    {
        this->accept();
    }
    else
    {
        QMessageBox::warning(this,"警告！","用户名或密码错误,请重新输入!");
        ui->lineEdit_userName->clear();//清除用户名
        ui->lineEdit_passwd->clear();//清除密码框
        ui->lineEdit_userName->setFocus();//用户名框设置焦点
    }
}
